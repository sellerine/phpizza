<div class="container-fluid mg-bt">
    <div class="row content">
        <form class="form-group col-sm-6" name="customerData" method="post">
            <h1 class="heading-l">Lieferadresse</h1>
            <?php if (!empty($messages)): ?>
                <ul>
                    <?php foreach ($messages as $message): ?>
                        <li><?= $message ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif ?>
            <fieldset class="row">
                <div class="col-sm-5">
                    <label>
                        Vor- und Zuname*:
                        <input class="form-control element-background content" type="text" id="nameField" name="name"
                               required placeholder="Max Mustermann">
                    </label>
                    <br/>
                    <label>
                        Straße*:
                        <input class="form-control element-background content" type="text" id="streetField"
                               name="street" required placeholder="Beispielstraße">
                    </label>
                    <br/>
                    <label>
                        Hausnummer*:
                        <input class="form-control element-background content" type="text" id="hnField"
                               name="housenumber" required placeholder="2">
                    </label>
                    <br/>
                </div>
                <div class="col-sm-6">
                    <label>
                        Postleitzahl*:
                        <select class="form-control element-background content" type="text" id="zcField" name="zipcode"
                                required>
                            <?php
                            /**
                             * @var Order $zipcodes
                             */
                            foreach ($zipcodes as $zipcode): ?>
                                <option value="<?= $zipcode['lieferort_plz'] ?>"><?= $zipcode['lieferort_plz'] ?></option>
                            <?php endforeach; ?>

                        </select>
                    </label>
                    <br/>
                    <label>
                        Etage:
                        <input class="form-control element-background content" type="text" id="levelField" name="level"
                               placeholder="2">
                    </label>
                    <br/>
                    <label>
                        Telefonnummer:
                        <input class="form-control element-background content" type="text" id="phoneField" name="phone"
                               placeholder="03461 1234567">
                    </label>
                    <br/>
                </div>
            </fieldset>
            <p id="requiredInfo">*erforderliche Angaben</p>
            <?php
            /**
             * @var Cart $cart
             */
            if (!empty($cart->getCart())): { ?>
                <input type="submit" class="btn btn-dark content" value="Bestellung abschicken">
                <a class="btn btn-dark content" href="?route=start">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle"
                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path fill-rule="evenodd"
                              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                    </svg>
                    Zurückgehen, um Ihre Auswahl zu bearbeiten

                </a>
            <?php } else : { ?>
                <a class="btn btn-dark content" href="?route=start">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle"
                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path fill-rule="evenodd"
                              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                    </svg>
                    Zurückgehen, um Produkte auszuwählen
                </a>
            <?php } endif; ?>
        </form>
        <div class="col-sm-6">
            <h1 class="heading-l">Warenkorb</h1>
            <table class="content table table-config table-striped table-sm">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Beschreibung</th>
                    <th scope="col">Anzahl</th>
                    <th scope="col">Preis</th>
                    <th scope="col">Anmerkungen</th>
                </tr>
                </thead>
                <tbody>
                <?php /**
                 * @var Cart $cart
                 */
                foreach ($cart->getCart() as $key => $item): ?>
                    <tr>
                        <td class="td tx-al-top"><?= $item->name ?></td>
                        <td class="td tx-al-top"> x <?= $item->count ?></td>
                        <td class="td tx-al-top table-no-br tx-al-rt"><?= $item->calculateSum() ?> €</td>
                        <td class="td tx-al-top"><?= isset($item->comment) ? $item->comment : '' ?></td>
                    </tr>

                <?php endforeach ?>
                <tr class="heading-s">
                    <td>Summe:</td>
                    <td colspan="2" class="tx-al-rt tx-sum"><?= $cart->caluculateTotal() ?> €</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <div>
                <br/>
                <br/>
                Zur Zeit ist nur Barzahlung möglich.
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cash" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M15 4H1v8h14V4zM1 3a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H1z"/>
                    <path d="M13 4a2 2 0 0 0 2 2V4h-2zM3 4a2 2 0 0 1-2 2V4h2zm10 8a2 2 0 0 1 2-2v2h-2zM3 12a2 2 0 0 0-2-2v2h2zm7-4a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>
                </svg>
            </div>
        </div>
    </div>
</div>

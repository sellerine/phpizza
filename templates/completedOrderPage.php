<div class="mg-lt mg-bt content">
    <h1 class="heading-l">Vielen Dank für Ihre Bestellung bei uns.</h1>
    <h2 class="heading-m">Hier sehen Sie nochmal Ihre Bestellung im Überblick:</h2>
    <div>
        <table class="table table-config table-striped table-sm">
            <thead class="thead-dark">
            <th>Kundendaten:</th>
            <th></th>
            </thead>
            <tbody>
            <tr>
                <td>Name:</td>
                <?php /**
                 * @var CompletedOrder $order
                 * @var CompletedOrder $city
                 */ ?>
                <td><?= $order['customer']['name'] ?></td>
            </tr>
            <tr>
                <td>Adresse:</td>
                <td>
                    <?= $order['customer']['street'] . ' ' . $order['customer']['housenumber'] . (!($order['customer']['level'] === null) ? ', ' . $order['customer']['level'] .
                        '. Etage' : '') ?>
                    <br/>
                    <?= $order['customer']['zipcode'] . ' ' . $city ?>
                </td>
            </tr>
            <?php if (!empty($order['customer']['phone'])): ?>
                <tr>
                    <td>Tel:</td>
                    <td><?= $order['customer']['phone'] ?></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div>
        <?php $total = 0;
        if (!empty($order['pizzas'])): ?>
            <table class="table table-config table-striped table-sm">
                <thead class="thead-dark">
                    <th colspan="5">Bestellte Pizzen:</th>
                </thead>
                <tbody>
                <?php foreach ($order['pizzas'] as $pizza): ?>
                    <tr>
                        <td><?= $pizza['count'] ?>x</td>
                        <td><?= $pizza['name'] ?></td>
                        <td>(<?= $pizza['size'] ?>)</td>
                        <td class="tx-al-rt"><?= $pizza['price'] * $pizza['count'] ?> €</td>
                        <td><?= !empty($pizza['comment']) ? ' - "' . $pizza['comment'] . '"' : '' ?></td>
                    </tr>
                    <?php $total += $pizza['price'] * $pizza['count'];
                endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <?php
        if (!empty($order['drinks'])): ?>
            <table class="table table-config table-striped table-sm">
                <thead class="thead-dark">
                <th colspan="3">Bestellte Getränke:</th>
                </thead>
                <tbody>
                <?php foreach ($order['drinks'] as $drink): ?>
                    <tr>
                        <td><?= $drink['count'] ?>x</td>
                        <td><?= $drink['name'] ?></td>
                        <td class="tx-al-rt"><?= $drink['price'] * $drink['count'] ?> €</td>
                    </tr>
                    <?php $total += $drink['price'] * $drink['count'];
                endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
        <br/>
        <div class="heading-s">
            Gesamtpreis:
            <span class="tx-sum">
                <?= $total ?> €
            </span>

        </div>
    </div>

    <div>
        <div>
            Zeitpunkt der Bestellung:
            <span class="heading-s">
                <?= $order['orderDate']->format('d.m.Y H:i') ?> Uhr
            </span>
        </div>
        <div>
            Die voraussichtliche Lieferdauer für Ihre Bestellung beträgt
            <span class="heading-s">
                <?= $order['deliveryTime'] ?> Minuten
            </span>
        </div>
    </div>
    <br/>

    <button class="btn btn-dark hidden-print hide" onclick="window.print()">Seite drucken</button>
</div>
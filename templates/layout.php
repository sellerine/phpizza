<!DOCTYPE html>
<html style="scroll-behavior: smooth">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="style.css?<?= time() ?>">
        <title>PHPizza - Pizzalieferservice</title>
    </head>
    <body class="body">
        <div id="headImg" class="printable-title">
            PHPizza
        </div>

        <?php
        /**
         * @var AbstractController $tpl
        */
        require $tpl ?>
        <script src="bootstrap/jquery-3.5.1.slim.min.js"></script>
        <script src="bootstrap/popper.min.js"></script>
        <script src="bootstrap/bootstrap.min.js"></script>
    </body>
</html>
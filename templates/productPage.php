<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark content">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Seitenanfang</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pizza">Pizzen</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#drink">Getränke</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#cart">Warenkorb</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#time">Adresse und Öffnungszeiten</a>
            </li>
        </ul>
    </div>
</nav>


<div class="container-fluid">
    <div class="heading-m">
        <?php /** @var Product $open
         * @var Product $pizzas
         * @var Product $drinks
         */
        ?>
        <a id="pizza">Wir haben gerade <?= $open ? 'für Sie geöffnet' : 'geschlossen' ?>.</a>
        <a id="cart"></a>
    </div>
    <div class="row">
        <div class="sortiment col-sm-7">
            <h1 class="heading-l">Pizzen</h1>
            <?php foreach ($pizzas as $pizzaName => $pizza): ?>
                <?php $pizzaID = $pizza[0]['pizza_id']; ?>
                <div class="card element-background">
                    <div class="card-header content"><?= $pizzaName ?></div>
                    <div class="card-body row">
                        <a class="col-sm-4" href="#showForm<?= $pizzaID ?>" data-toggle="collapse">
                            <img class="productPic" src="img/pizzen/<?= $pizzaName ?>.jpg">
                        </a>
                        <form name="chooseSize" method="post" id="showForm<?= $pizzaID ?>" class="collapse col-sm-3">
                            <fieldset>
                                <br/>
                                <?php foreach ($pizza as $pizzaSize): ?>
                                    <label class="content">
                                        <input name="pizzaID" type="radio" value="<?= $pizzaSize['pizza_id'] ?>"
                                               required>
                                        <?= strtoupper($pizzaSize['pizza_groesse']) ?>
                                        - <?= $pizzaSize['pizza_preis'] ?> €
                                    </label>
                                    <br/>
                                <?php endforeach ?>
                                <input class="btn btn-dark content" name="putIntoCart" type="submit"
                                       value="dem Warenkorb hinzufügen">
                            </fieldset>
                        </form>
                    </div>
                </div>
            <?php endforeach ?>
            <a id="drink"><h1 class="heading-l mg-top">Getränke</h1></a>
            <?php foreach ($drinks as $drink): ?>
                <div class="card element-background">
                    <div class="card-header content"><?= $drink['getraenk_name'] ?></div>
                    <div class="card-body row">
                        <form name="chooseDrink" method="post">
                            <fieldset>
                                <div class="content margin"><?= $drink['getraenk_preis'] ?> €</div>
                                <input type="hidden" name="drinkID" value="<?= $drink['getraenk_id'] ?>">
                                <input class="btn btn-dark content" name="putIntoCart" type="submit"
                                       value="dem Warenkorb hinzufügen">
                            </fieldset>
                        </form>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <div class="cart col-sm-5 pd-lt">
            <h1 class="heading-l">Warenkorb</h1>
            <table class="content">
                <?php /* @var Cart $cart */ ?>
                <?php foreach ($cart->getCart() as $key => $item): ?>
                    <tr>
                        <td><?= $item->name ?></td>
                        <td><a class="btn btn-dark btn-sm countButton" href="?decreaseCount=<?= $key ?>">-</a></td>
                        <td class="tx-al"><?= $item->count ?></td>
                        <td><a class="btn btn-dark btn-sm countButton" href="?increaseCount=<?= $key ?>">+</a></td>
                        <td class="tx-al-rt table-no-br"><?= $item->calculateSum() ?> €</td>
                    </tr>
                    <?php if (isset($item->pizzaID)): ?>
                        <tr>
                            <td>
                                <form method="post" class="form-group">
                                    <input type="hidden" name="cartKey" value="<?= $key ?>">
                                    <textarea class="textArea form-control"
                                              name="comment"><?= isset($item->comment) ? $item->comment : '' ?></textarea>
                                    <input type="submit" class="btn btn-dark content" value="Anmerkung hinzufügen">
                                </form>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach ?>
                <tr>
                    <td colspan="5" role="separator" class="dropdown-divider line"></td>
                </tr>
                <tr class="heading-s">
                    <td>Summe: </td>
                    <td colspan="4" class="tx-sum tx-al-rt"> <?= $cart->caluculateTotal() ?> €</td>
                </tr>
            </table>
            <?php if (!empty($cart->getCart()) && $open): ?>
                <a href="?route=order">
                    <button class="btn btn-dark content">
                        weiter
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle"
                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd"
                                  d="M4 8a.5.5 0 0 0 .5.5h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5A.5.5 0 0 0 4 8z"/>
                        </svg>
                    </button>
                </a>
            <?php endif ?>
        </div>
    </div>
</div>
<div class="mg-bt">
    <div class="container-fluid">
        <a id="time"><h1 class="heading-l mg-top">Adresse und Öffnungszeiten</h1></a>
        <div class="row">
            <div class="col-sm-2 content tx-al">
                <div class="heading-m">
                    Adresse
                </div>
                <div>
                    Entenplan
                    <br/>
                    06217 Merseburg
                    <br/>
                    Tel.: 0123/456789
                </div>
            </div>
            <div class="col-sm-3 content tx-al">
                <div class="heading-m">
                    Öffnungszeiten
                </div>
                <div>
                    Montag – Freitag 10:00 – 22:00 Uhr
                    <br/>
                    Samstag 10:00 – 22:00 Uhr
                    <br/>
                    Sonntag 10:00 – 22:00 Uhr
                </div>
            </div>
            <div class="col-sm-3">
                <div class="heading-m">
                    Anfahrt
                </div>
                <div>
                    <iframe id="map"
                            src=https://maps.google.de/maps?hl=de&q=%20Entenplan%20Merseburg&t=&z=14&ie=utf8&iwloc=b&output=embed
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content margin">
    <div class="heading-m">
        Impressum:
    </div>
    <div role="separator" class="dropdown-divider line mg-rt"></div>
    Selina B. - Mat.-Nr.: XXXXX
    <br/>
    Tim B. - Mat.-Nr.: XXXXX
</div>




-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 07. Sep 2020 um 18:00
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `phpizza`
--
CREATE DATABASE IF NOT EXISTS `phpizza` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `phpizza`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellung`
--

CREATE TABLE `bestellung` (
  `bestell_id` int(11) NOT NULL,
  `bestell_datum` timestamp NOT NULL DEFAULT current_timestamp(),
  `bestell_lieferdauer` int(11) DEFAULT 20,
  `kunden_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getraenk`
--

CREATE TABLE `getraenk` (
  `getraenk_id` int(11) NOT NULL,
  `getraenk_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `getraenk_preis` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `getraenk`
--

INSERT INTO `getraenk` (`getraenk_id`, `getraenk_name`, `getraenk_preis`) VALUES
(1, 'Cola', '1.00'),
(2, 'Fanta', '1.00'),
(3, 'Sprite', '1.00'),
(4, 'Mineralwasser mit Kohlensäure', '0.70'),
(5, 'Mineralwasser ohne Kohlensäure', '0.50'),
(6, 'Bier', '2.00'),
(7, 'Apfelsaft', '1.20'),
(8, 'Orangensaft', '1.20');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getraenkebestellung`
--

CREATE TABLE `getraenkebestellung` (
  `getraenk_id` int(11) NOT NULL,
  `bestell_id` int(11) NOT NULL,
  `getraenkebestell_anzahl` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunde`
--

CREATE TABLE `kunde` (
  `kunden_id` int(11) NOT NULL,
  `kunden_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kunden_strasse` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kunden_hausnummer` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `kunden_telefon` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kunden_etage` int(11) DEFAULT NULL,
  `kunden_plz` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `kunde`
--

INSERT INTO `kunde` (`kunden_id`, `kunden_name`, `kunden_strasse`, `kunden_hausnummer`, `kunden_telefon`, `kunden_etage`, `kunden_plz`) VALUES
(1, 'testkunde', 'abc', '02b', '0765432', 2, '06217');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lieferort`
--

CREATE TABLE `lieferort` (
  `lieferort_plz` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lieferort_ort` varchar(45),
  `lieferort_entfernung` int(11) NOT NULL,
  `lieferort_lieferdauer` int(11) DEFAULT 20
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `lieferort` (`lieferort_plz`, `lieferort_ort`, `lieferort_entfernung`, `lieferort_lieferdauer`) VALUES
('06217', 'Merseburg', 3, 7),
('06231', 'Bad Dürrenberg', 15, 25),
('06237', 'Leuna', 12, 18),
('06242', 'Braunsbedra', 23, 48),
('06246', 'Bad Lauchstädt', 20, 34),
('06258', 'Schkopau', 10, 15);
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pizza`
--

CREATE TABLE `pizza` (
  `pizza_id` int(11) NOT NULL,
  `pizza_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pizza_groesse` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `pizza_preis` decimal(5,2) NOT NULL,
  `pizza_zubereitungsdauer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `pizza`
--

INSERT INTO `pizza` (`pizza_id`, `pizza_name`, `pizza_groesse`, `pizza_preis`, `pizza_zubereitungsdauer`) VALUES
(1, 'Margherita', 'S', '5.00', 2),
(2, 'Margherita', 'M', '6.00', 2),
(3, 'Margherita', 'L', '7.00', 2),
(4, 'Salami', 'S', '5.50', 3),
(5, 'Salami', 'M', '6.50', 3),
(6, 'Salami', 'L', '7.50', 3),
(7, 'Prosciutto', 'S', '6.00', 3),
(8, 'Prosciutto', 'M', '7.00', 3),
(9, 'Prosciutto', 'L', '8.00', 3),
(10, 'Funghi', 'S', '5.50', 3),
(11, 'Funghi', 'M', '6.50', 3),
(12, 'Funghi', 'L', '7.50', 3),
(13, 'Peperoni', 'S', '6.00', 3),
(14, 'Peperoni', 'M', '7.00', 3),
(15, 'Peperoni', 'L', '8.00', 3),
(16, 'Quattro Stagioni', 'S', '7.00', 6),
(17, 'Quattro Stagioni', 'M', '8.00', 6),
(18, 'Quattro Stagioni', 'L', '9.00', 6),
(19, 'Tonno', 'S', '7.00', 4),
(20, 'Tonno', 'M', '8.00', 4),
(21, 'Tonno', 'L', '9.00', 4),
(22, 'Hawaii', 'S', '6.00', 4),
(23, 'Hawaii', 'M', '7.00', 4),
(24, 'Hawaii', 'L', '8.00', 4),
(25, 'Frutti di Mare', 'S', '9.00', 3),
(26, 'Frutti di Mare', 'M', '10.00', 3),
(27, 'Frutti di Mare', 'L', '11.00', 3),
(28, 'Bolognese', 'S', '7.00', 4),
(29, 'Bolognese', 'M', '8.00', 4),
(30, 'Bolognese', 'L', '9.00', 4),
(31, 'Calzone', 'S', '7.50', 7),
(32, 'Calzone', 'M', '8.50', 7),
(33, 'Calzone', 'L', '9.50', 7),
(34, 'Napoli', 'S', '8.00', 4),
(35, 'Napoli', 'M', '9.00', 4),
(36, 'Napoli', 'L', '10.00', 4),
(37, 'Verdi', 'S', '6.50', 4),
(38, 'Verdi', 'M', '7.50', 4),
(39, 'Verdi', 'L', '8.50', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pizzabestellung`
--

CREATE TABLE `pizzabestellung` (
  `pizza_id` int(11) NOT NULL,
  `bestell_id` int(11) NOT NULL,
  `pizzabestell_anzahl` int(11) NOT NULL DEFAULT 1,
  `pizzabestell_anmerkung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  ADD PRIMARY KEY (`bestell_id`),
  ADD KEY `kunden_id_idx` (`kunden_id`);

--
-- Indizes für die Tabelle `getraenk`
--
ALTER TABLE `getraenk`
  ADD PRIMARY KEY (`getraenk_id`);

--
-- Indizes für die Tabelle `getraenkebestellung`
--
ALTER TABLE `getraenkebestellung`
  ADD PRIMARY KEY (`getraenk_id`,`bestell_id`),
  ADD KEY `bestell_id_idx` (`bestell_id`);

--
-- Indizes für die Tabelle `kunde`
--
ALTER TABLE `kunde`
  ADD PRIMARY KEY (`kunden_id`);

--
-- Indizes für die Tabelle `lieferort`
--
ALTER TABLE `lieferort`
  ADD PRIMARY KEY (`lieferort_plz`);

--
-- Indizes für die Tabelle `pizza`
--
ALTER TABLE `pizza`
  ADD PRIMARY KEY (`pizza_id`);

--
-- Indizes für die Tabelle `pizzabestellung`
--
ALTER TABLE `pizzabestellung`
  ADD PRIMARY KEY (`pizza_id`,`bestell_id`),
  ADD KEY `bestell_id_idx` (`bestell_id`),
  ADD KEY `pizza_id_idx` (`pizza_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  MODIFY `bestell_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `getraenk`
--
ALTER TABLE `getraenk`
  MODIFY `getraenk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `kunde`
--
ALTER TABLE `kunde`
  MODIFY `kunden_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `pizza`
--
ALTER TABLE `pizza`
  MODIFY `pizza_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  ADD CONSTRAINT `kunden_id` FOREIGN KEY (`kunden_id`) REFERENCES `kunde` (`kunden_id`);

--
-- Constraints der Tabelle `getraenkebestellung`
--
ALTER TABLE `getraenkebestellung`
  ADD CONSTRAINT `fk_getraenkebestell_bestell_id` FOREIGN KEY (`bestell_id`) REFERENCES `bestellung` (`bestell_id`),
  ADD CONSTRAINT `fk_getraenkebestell_getraenk_id` FOREIGN KEY (`getraenk_id`) REFERENCES `getraenk` (`getraenk_id`);

--
-- Constraints der Tabelle `pizzabestellung`
--
ALTER TABLE `pizzabestellung`
  ADD CONSTRAINT `fk_pizzabestell_bestell_id` FOREIGN KEY (`bestell_id`) REFERENCES `bestellung` (`bestell_id`),
  ADD CONSTRAINT `fk_pizzabestell_pizza_id` FOREIGN KEY (`pizza_id`) REFERENCES `pizza` (`pizza_id`);



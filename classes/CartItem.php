<?php


class CartItem
{
    public int $pizzaID;
    public int $drinkID;
    public float $price;
    public string $name;  //+size
    public int $count = 1;
    public string $comment = '';

    public function calculateSum ()
    {
        return $this->price * $this->count;
    }
}
<?php


class Database
{
    private $_pdo;

    public function __construct()
    {
        $config = (require 'config/config.php')['mysql'];
        $this->_pdo = new PDO(
            'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . ';charset=utf8',
            $config['username'],
            $config['password'],
            [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
        );
    }

    public function getPizzas(): array
    {
        //Query vorbereiten
        $pizzaStmt = $this->_pdo->prepare('SELECT pizza_id,pizza_name,pizza_groesse,pizza_preis FROM pizza ');

        //Query auswerten
        if (!$pizzaStmt->execute()) {
            throw new \Exception("Keine Pizzen im Sortiment");
        }
        return $pizzaStmt->fetchAll();
    }

    public function getDrinks(): array
    {
        //Query vorbereiten
        $drinkStmt = $this->_pdo->prepare('SELECT * FROM getraenk');


        //Query auswerten
        if (!$drinkStmt->execute()) {
            throw new \Exception("Keine Getränke im Sortiment");
        }
        return $drinkStmt->fetchAll();
    }

    public function findCustomer(string $name, string $street, string $housenumber, ?int $level, string $zipcode): ?int
    {
        //Query vorbereiten
        $customerStmt = $this->_pdo->prepare(
            "SELECT * FROM kunde WHERE kunden_name = '$name' AND kunden_strasse = '$street' AND kunden_hausnummer = '$housenumber' AND kunden_etage = '$level' AND kunden_plz = '$zipcode'");

        $result = $customerStmt->fetch();
        //Query auswerten
        if (!$result) {
            return null;
        }
        return $result['kunden_id'];
    }

    public function createCustomer(string $name, string $street, string $housenumber, ?int $level, string $zipcode, ?string $phone): int
    {
        //Query vorbereiten
        $customerStmt = $this->_pdo->prepare('INSERT INTO kunde (kunden_name, kunden_strasse, kunden_hausnummer, kunden_etage, kunden_plz, kunden_telefon) VALUES (:name, :street, :housenumber, :level, :zipcode, :phone)');

        //Query auswerten
        $customerStmt->execute([
            'name'        => $name,
            'street'      => $street,
            'housenumber' => $housenumber,
            'level'       => $level,
            'zipcode'     => $zipcode,
            'phone'       => $phone
        ]);

        return $this->_pdo->lastInsertId();
    }

    public function updateCustomer(int $customerID, ?string $phone): void
    {
        //Query vorbereiten
        $customerStmt = $this->_pdo->prepare(
            'UPDATE kunde SET kunden_telefon=:phone WHERE kunden_id = :customerID');

        //Query auswerten
        $customerStmt->execute([
            'phone'      => $phone,
            'customerID' => $customerID
        ]);

    }

    public function createOrder(int $deliveryTime, int $customerID): int
    {
        //Query vorbereiten
        $orderStmt = $this->_pdo->prepare(
            'INSERT INTO bestellung (bestell_lieferdauer, kunden_id) VALUES (:deliveryTime, :customerID)');

        //Query auswerten
        $orderStmt->execute([
            'deliveryTime' => $deliveryTime,
            'customerID'   => $customerID
        ]);
        return $this->_pdo->lastInsertId();
    }

    public function createPizzaOrder(int $pizzaID, int $orderID, int $count, string $comment): void
    {
        //Query vorbereiten
        $pizzaOrderStmt = $this->_pdo->prepare(
            'INSERT INTO pizzabestellung (pizza_id, bestell_id, pizzabestell_anzahl, pizzabestell_anmerkung) VALUES (:pizzaID, :orderID, :count, :comment)');

        //Query auswerten
        $pizzaOrderStmt->execute([
            'pizzaID' => $pizzaID,
            'orderID' => $orderID,
            'count'   => $count,
            'comment' => $comment
        ]);
    }

    public function createDrinkOrder(int $drinkID, int $orderID, int $count): void
    {
        //Query vorbereiten
        $drinkOrderStmt = $this->_pdo->prepare(
            'INSERT INTO getraenkebestellung (getraenk_id, bestell_id, getraenkebestell_anzahl) VALUES (:drinkID, :orderID, :count)');

        //Query auswerten
        $drinkOrderStmt->execute([
            'drinkID'   => $drinkID,
            'orderID'   => $orderID,
            'count'     => $count
        ]);
    }

    public function getCompleteOrder(int $orderID): array
    {
        //Query vorbereiten
        $completeOrderStmt = $this->_pdo->prepare(
            "SELECT b.bestell_datum, b.bestell_lieferdauer, pb.pizzabestell_anzahl,
            pb.pizzabestell_anmerkung, gb.getraenkebestell_anzahl, k.kunden_name, k.kunden_plz, k.kunden_strasse,
            k.kunden_hausnummer, k.kunden_telefon, k.kunden_etage, p.pizza_name, p.pizza_groesse, p.pizza_preis,
            g.getraenk_name, g.getraenk_preis, pb.pizza_id, gb.getraenk_id FROM bestellung b
            LEFT JOIN pizzabestellung pb ON pb.bestell_id=b.bestell_id
            LEFT JOIN getraenkebestellung gb ON gb.bestell_id=b.bestell_id
            LEFT JOIN kunde k ON b.kunden_id=k.kunden_id
            LEFT JOIN pizza p ON pb.pizza_id=p.pizza_id
            LEFT JOIN getraenk g ON gb.getraenk_id=g.getraenk_id
            WHERE b.bestell_id='$orderID'");


        //Query auswerten
        if (!$completeOrderStmt->execute()) {
            throw new \Exception("Das ist nicht die Bestellung nach der du suchst.");
        }
        $result = $completeOrderStmt->fetchAll();

        $order = [
            'customer'        => [
                'name'        => $result[0]['kunden_name'],
                'street'      => $result[0]['kunden_strasse'],
                'housenumber' => $result[0]['kunden_hausnummer'],
                'zipcode'     => $result[0]['kunden_plz'],
                'level'       => $result[0]['kunden_etage'],
                'phone'       => $result[0]['kunden_telefon']
            ],
            'deliveryTime' => $result[0]['bestell_lieferdauer'],
            'orderDate'    => new DateTime($result[0]['bestell_datum']),
            'pizzas'       => [],
            'drinks'       => []
        ];

        foreach ($result as $row) {
            if ($row['pizza_id'] !== null && !isset($order['pizzas'][$row['pizza_id']])) {
                $order['pizzas'][$row['pizza_id']] = [
                    'name'    => $row['pizza_name'],
                    'size'    => $row['pizza_groesse'],
                    'price'   => $row['pizza_preis'],
                    'count'   => $row['pizzabestell_anzahl'],
                    'comment' => $row['pizzabestell_anmerkung']
                ];
            }
            if ($row['getraenk_id'] !== null && !isset($order['drinks'][$row['getraenk_id']])) {
                $order['drinks'][$row['getraenk_id']] = [
                    'name'  => $row['getraenk_name'],
                    'price' => $row['getraenk_preis'],
                    'count' => $row['getraenkebestell_anzahl']
                ];
            }
        }

        return $order;
    }

    public function findZipcode(string $zipcode): bool
    {
        //Query vorbereiten
        $zipcodeStmt = $this->_pdo->prepare(
            "SELECT lieferort_plz FROM lieferort WHERE lieferort_plz = '$zipcode'");

        $zipcodeStmt->execute();
        $deliverable = $zipcodeStmt->fetch();
        return (bool)$deliverable;
    }

    public function getAllZipcodes(): array
    {
        //Query vorbereiten
        $zipcodeStmt = $this->_pdo->prepare(
            "SELECT lieferort_plz FROM lieferort");

        $zipcodeStmt->execute();
        return $zipcodeStmt->fetchAll();
    }

    public function findDeliveryTimeByZipcode(string $zipcode): int
    {
        //Query vorbereiten
        $Stmt = $this->_pdo->prepare(
            "SELECT lieferort_lieferdauer FROM lieferort WHERE lieferort_plz = '$zipcode'");

        $Stmt->execute();
        $result = $Stmt->fetch();
        if (!$result) {
            throw new Exception("Unbekannte Lieferdauer");
        }

        return $result['lieferort_lieferdauer'];
    }

    public function findPizzaProcessingTimeById(int $pizzaID): int
    {
        //Query vorbereiten
        $Stmt = $this->_pdo->prepare(
            "SELECT pizza_zubereitungsdauer FROM pizza WHERE pizza_id = '$pizzaID'");

        $Stmt->execute();
        $result = $Stmt->fetch();

        return $result['pizza_zubereitungsdauer'] ?? 5;
    }

    public function findCityByZipcode(string $zipcode): string
    {
        //Query vorbereiten
        $Stmt = $this->_pdo->prepare(
            "SELECT lieferort_ort FROM lieferort WHERE lieferort_plz = '$zipcode'");

        $Stmt->execute();
        $result = $Stmt->fetch();

        return $result['lieferort_ort'] ?? "";
    }

}

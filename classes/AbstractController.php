<?php


abstract class AbstractController
{
    protected $_database;

    public function __construct(Database $database)
    {
        $this->_database = $database;
    }

    abstract public function run();

}
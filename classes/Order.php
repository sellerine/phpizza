<?php
require_once 'AbstractController.php';

class Order extends AbstractController
{
    protected $messages = [];

    public function run()
    {
        $cart = new Cart;
        $zipcodes = $this->_database->getAllZipcodes();

        if (!empty($_POST)) {
            $post = $this->validate($_POST);
            $messages = $this->messages;
            if (empty($messages)) {
                $customerID = $this->_database->findCustomer($post['name'], $post['street'], $post['housenumber'], $post['level'], $post['zipcode']);
                if ($customerID === null) {
                    $customerID = $this->_database->createCustomer($post['name'], $post['street'], $post['housenumber'], $post['level'], $post['zipcode'], $post['phone']);
                } else {
                    $this->_database->updateCustomer($customerID, $post['phone']);
                }

                $orderID = $this->_database->createOrder($this->calcDeliveryTime($cart, $post['zipcode']), $customerID);

                foreach ($cart->getCart() as $item) {
                    if (isset($item->pizzaID)) {
                        $this->_database->createPizzaOrder($item->pizzaID, $orderID, $item->count, $item->comment);
                    } elseif (isset($item->drinkID)) {
                        $this->_database->createDrinkOrder($item->drinkID, $orderID, $item->count);
                    }
                }
                $_SESSION['orderID'] = $orderID;
                header('Location: index.php?route=complete');
            }
        }

        $tpl = 'templates/orderPage.php';
        require 'templates/layout.php';
    }

    protected function validate(array $post): array
    {
        $level = trim($post['level']);
        if ($level === "") {
            $post['level'] = null;
        } elseif (!is_numeric($level)) {
            $this->messages[] = 'Bitte geben Sie statt "' . $level . '" nur die Nummer der Etage an.';
        } else {
            $post['level'] = (int)$level;
        }

        if (!$this->_database->findZipcode($post['zipcode'])) {
            $this->messages[] = "Zu der von Ihnen angegebenen Postleitzahl (" . $post['zipcode'] . ") können wir leider nicht liefern.";
        }
        if (empty($post['phone'])) {
            $post['phone'] = null;
        }
        return $post;
    }

    protected function calcDeliveryTime(Cart $cart, string $zipcode): int
    {
        $deliveryTime = $this->_database->findDeliveryTimeByZipcode($zipcode);
        foreach ($cart->getCart() as $item) {
            if (isset($item->pizzaID)) {
                $deliveryTime += $item->count * $this->_database->findPizzaProcessingTimeById($item->pizzaID);
            }
        }
        return $deliveryTime;
    }

}
<?php
require_once 'AbstractController.php';

class CompletedOrder extends AbstractController
{
    public function run()
    {

        $order = $this->_database->getCompleteOrder($_SESSION['orderID']);
        $city = $this->_database->findCityByZipcode($order['customer']['zipcode']);
        $tpl = 'templates/completedOrderPage.php';
        require 'templates/layout.php';
    }

}
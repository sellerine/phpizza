<?php
require_once 'AbstractController.php';
require_once 'Cart.php';

class Product extends AbstractController
{

    public function run()
    {
        $ungroupedPizzas = $this->_database->getPizzas();
        $pizzas = $this->groupPizzas($ungroupedPizzas);
        $drinks = $this->_database->getDrinks();
        $cart = new Cart();
        $worktimestart = strtotime('10:00:00');
        $worktimeend = strtotime('22:00:00');
        $open = (time() >= $worktimestart && time() <= $worktimeend);

        date_default_timezone_set("Europe/Berlin");

        if (!empty($_POST)) {
            if (isset($_POST['pizzaID'])) {
                $pizza = $this->findPizzaById($ungroupedPizzas, $_POST['pizzaID']);
                $newItem = new CartItem();
                $newItem->pizzaID = $_POST['pizzaID'];
                $newItem->name = $pizza['pizza_name'] . ' (' . strtoupper($pizza['pizza_groesse']) . ')';
                $newItem->price = $pizza['pizza_preis'];
                $cart->addItem($newItem);
            } elseif (isset($_POST['drinkID'])) {
                $drink = $this->findDrinkById($drinks, $_POST['drinkID']);
                $newItem = new CartItem();
                $newItem->drinkID = $_POST['drinkID'];
                $newItem->name = $drink['getraenk_name'];
                $newItem->price = $drink['getraenk_preis'];
                $cart->addItem($newItem);
            } elseif (isset($_POST['cartKey'])) {
                $cart->addComment($_POST['cartKey'], $_POST['comment']);
            }
            header('Location: index.php');
        }
        if (isset($_GET['increaseCount'])) {
            $cart->increaseCount($_GET['increaseCount']);
            header('Location: index.php#cart');
        }
        if (isset($_GET['decreaseCount'])) {
            $cart->decreaseCount($_GET['decreaseCount']);
            header('Location: index.php#cart');
        }
        $tpl = 'templates/productPage.php';
        require 'templates/layout.php';
    }

    protected function groupPizzas(array $pizzas): array
    {
        $groupedPizzas = [];
        foreach ($pizzas as $pizza) {
            if (!isset($groupedPizzas[$pizza['pizza_name']])) {
                $groupedPizzas[$pizza['pizza_name']] = [];
            }
            $groupedPizzas[$pizza['pizza_name']][] = $pizza;
        }
        return $groupedPizzas;
    }

    protected function findPizzaById(array $pizzas, int $pizzaID): array
    {
        foreach ($pizzas as $pizza) {
            if ($pizza['pizza_id'] == $pizzaID) {
                return $pizza;
            }
        }
        throw new Exception('Pizza nicht gefunden');
    }

    protected function findDrinkById(array $drinks, int $drinkID): array
    {
        foreach ($drinks as $drink) {
            if ($drink['getraenk_id'] == $drinkID) {
                return $drink;
            }
        }
        throw new Exception('Getränk nicht gefunden');
    }
}

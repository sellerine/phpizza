<?php
require_once 'AbstractController.php';

class NotFound extends AbstractController
{

    public function run()
    {
        $tpl = 'templates/notFound.php';
        require 'templates/layout.php';
    }
}
<?php

require_once 'CartItem.php';

class Cart
{
    /* @var CartItem[] */
    private $cart = [];

    public function __construct()
    {
        if (isset($_SESSION['cart'])) {
            $this->cart = unserialize($_SESSION['cart']);
        }
    }

    /**
     * @return CartItem[]
     */
    public function getCart(): array
    {
        return $this->cart;
    }

    public function caluculateTotal(): float
    {
        $total = 0;
        foreach ($this->cart as $item) {
            $total += $item->calculateSum();
        }
        return $total;
    }

    public function increaseCount(int $index)
    {
        $this->cart[$index]->count++;
        $this->save();
    }

    public function decreaseCount(int $index)
    {
        $this->cart[$index]->count--;
        if ($this->cart[$index]->count < 1) {
            unset($this->cart[$index]);
        }
        $this->save();
    }

    public function addItem(CartItem $product)
    {
        if (isset($product->pizzaID)) {
            $identifier = 'pizzaID';
        } elseif (isset($product->drinkID)) {
            $identifier = 'drinkID';
        }
        /* @var CartItem $item */
        $matchItem = false;
        foreach ($this->cart as $key => $item) {
            if (isset($item->{$identifier}) && $product->{$identifier} === $item->{$identifier}) {
                $item->count++;
                $matchItem = true;
                break;
            }
        }
        if (!$matchItem) {
            $this->cart[] = $product;
        }
        $this->save();
    }

    public function removeItem(CartItem $product)
    {

        if ($product->pizzaID !== null) {
            $identifier = 'pizzaID';
        } elseif ($product->drinkID !== null) {
            $identifier = 'drinkID';
        }
        /* @var CartItem $item */
        foreach ($this->cart as $key => $item) {
            if ($product->{$identifier} === $item->{$identifier}) {
                if ($item->count > 1) {
                    $item->count--;
                } else {
                    unset($this->cart[$key]);
                }

                $this->save();
                break;
            }
        }
    }


    public function addComment(int $key, string $comment): void
    {
        $this->cart[$key]->comment = $comment;
        $this->save();
    }

    protected function save()
    {
        $_SESSION['cart'] = serialize($this->cart);
    }


}


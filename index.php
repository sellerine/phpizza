<?php

session_start();
require_once "./classes/Database.php";
require_once "./classes/Product.php";
require_once "./classes/Order.php";
require_once "./classes/NotFound.php";
require_once "./classes/Cart.php";
require_once "./classes/CompletedOrder.php";

$routes = [
    'start'     => Product::class,
    'order'     => Order::class,
    '404'       => NotFound::class,
    'complete'  => CompletedOrder::class
];

$route = $routes[$_GET['route'] ?? 'start'] ?? $routes['404'];    //wenn Route gesetzt ist, dann $route = $_GET['route'], sonst $route = $routes['start']

$database   = new Database();
/* @var AbstractController $controller */
$controller = new $route($database);

$controller->run();
if($route == 'CompletedOrder')
{
    unset($_SESSION['cart']);
}
